<?php
session_start();
require_once("../../vendor/autoload.php");
include('../templateLayout/information.php');
use App\model\AuthorMaster;
use App\Utility\Utility;
if($_SESSION['role_status']==2){
    $auth= new AuthorMaster();
    $status = $auth->prepareData($_SESSION)->logged_in();
    if(!$status) {
        Utility::redirect('../signIn.php');
        return;
    }
}

else {
    Utility::redirect('../signIn.php');
}
use App\model\AuthorUpPaper;
use App\model\ReviewerActivity;
$object=new AuthorUpPaper();
$object->prepareData($_GET);
$oneData = $object->showpaper();
$review= new ReviewerActivity();
$review->prepareData($_GET);
$allData=$review->showcomment();
$rating=$review->showrating();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="Free Bootstrap Themes by 365Bootstrap dot com - Free Responsive Html5 Templates">
    <meta name="author" content="http://www.365bootstrap.com">

    <title><?php echo $title?></title>

    <!-- Bootstrap Core CSS -->
    <?php require_once('../templateLayout/templateCSS.php');?>
    <?php require_once('../templateLayout/tableCss.php');?>

</head>

<body>
<header>
    <?php require_once('admin_menu.php');?>
</header>


<!-- /////////////////////////////////////////Content -->
<div id="page-content" class="index-page container">
    <div class="row">
        <div id="main-content"><!-- background not working -->
            <div class="col-md-12">
                <div class="box">
                    <?php

                    use App\Message\Message;


                    if(isset($_SESSION) && !empty($_SESSION['message'])) {

                        $msg = Message::getMessage();

                        echo "
                        <div class='container'>
                            <div class='row'>
                                <div class='col-md-8 col-md-offset-2'>
                                    <p id='message' style='color: black; text-align: center; font-family: 'Times New Roman'; font-weight: 200 ;font-size: 20px;'><b>$msg</b></p>
                                </div>
                            </div>
                        </div>";
                    }

                    ?>
                    <div class="box-header header-vimeo">
                        <h2>Review</h2>
                    </div>
                    <div class="box-content">
                        <div class="row" style="min-height: 400px;">
                            <div class="col-md-6" >
                                <div class="thumbnail">
                                    <object data="../../resources/papers/<?php echo $oneData->file_path?>" type="application/pdf" width="100%" height="600px">
                                    </object>
                                    <p style="text-align: center"><a href="../../resources/papers/<?php echo $oneData->file_path?>" target="_blank"><?php echo $oneData->topic_title?></a></p>
                                </div>
                            </div>
                            <div class="col-md-6" >

                                <div class="col-md-12">
                                    <h3>
                                        Average rating of this paper :  <?php
                                        $vale= sprintf ("%.2f", $rating->avg_rating );
                                        echo $vale?>
                                    </h3>
                                    <?php
                                    $ratingwidth2=($rating->avg_rating)*20;
                                    $rating2=($vale);
                                    echo "
                                <div class=\"col-md-12\">
                                    <div class='col-md-6 col-md-offset-3'>
                                         <div class='progress '>
                                            <div class='progress-bar progress-bar-danger' role='progressbar' aria-valuenow='50' aria-valuemin='0' aria-valuemax='100' style='width:$ratingwidth2%'>
                                                $rating2  (Out of 5)
                                            </div>
                                        </div>
                                    </div></div>";
                                    ?>


                                    <h3>
                                        Previous Comments
                                    </h3>
                                    <div class="dataTable_wrapper">
                                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                            <thead>
                                            <tr>
                                                <th style="text-align: center; width: 30%">Name</th>
                                                <th style="text-align: center; width: 50%">Comment</th>
                                                <th style="text-align: center; width: 20%">Rating</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach ($allData as $b){
                                                $ratingwidth=($b->rat)*20;
                                                $rating=($b->rat);
                                                echo "
                                            <tr>
                                            <td>$b->name</td>
                                            <td>$b->com</td>
                                            <td><div class='progress '>
                                                            <div class='progress-bar progress-bar-info' role='progressbar' aria-valuenow='50' aria-valuemin='0' aria-valuemax='100' style='width:$ratingwidth%'>
                                                                $rating  (Out of 5)
                                                            </div>
                                                        </div></td>
                                        </tr>
                                            ";

                                            }

                                            ?>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>




                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Footer -->
<?php require_once('../templateLayout/footer.php');?>
<!-- Footer -->
<!--script-->

<?php require_once('../templateLayout/templateScript.php');?>
<?php require_once('../templateLayout/tableScript.php');?>
<script>
    $(document).on('ready', function(){
        $('#input-3').rating({displayOnly: true, step: 0.5});
    });
</script>
<!--script-->
</body>
</html>
