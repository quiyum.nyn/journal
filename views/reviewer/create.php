<?php
require_once("../../vendor/autoload.php");
require_once('../templateLayout/information.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="Free Bootstrap Themes by 365Bootstrap dot com - Free Responsive Html5 Templates">
    <meta name="author" content="http://www.365bootstrap.com">

    <title><?php echo $title?></title>

    <!-- Bootstrap Core CSS -->
    <?php require_once('../templateLayout/templateCSS.php');?>
</head>

<body>
<header>
    <?php require_once('../templateLayout/navigation.php');?>
</header>


<!-- /////////////////////////////////////////Content -->
<div id="page-content" class="index-page container">
    <div class="row">
        <div id="main-content"><!-- background not working -->
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header header-vimeo">
                        <h2>Sign Up</h2>
                    </div>
                    <div class="box-content">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
                                <form class="form-horizontal" action="">

                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="email">Email:</label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" id="email" placeholder="Enter Your email" name="email" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="pwd">Password:</label>
                                        <div class="col-sm-10">
                                            <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" class="btn btn-info">Sign Up</button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Footer -->
<?php require_once('../templateLayout/footer.php');?>
<!-- Footer -->
<!--script-->
<?php require_once('../templateLayout/templateScript.php');?>
<!--script-->
</body>
</html>
