<?php
session_start();
require_once("../vendor/autoload.php");
require_once('templateLayout/information.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="Free Bootstrap Themes by 365Bootstrap dot com - Free Responsive Html5 Templates">
    <meta name="author" content="http://www.365bootstrap.com">

    <title><?php echo $title?></title>

    <!-- Bootstrap Core CSS -->
    <?php require_once('templateLayout/templateCSS.php');?>
</head>

<body>
<header>
    <?php require_once('templateLayout/navigation.php');?>
</header>


<!-- /////////////////////////////////////////Content -->
<div id="page-content" class="index-page container">
    <div class="row">
        <div id="sidebar">
            <div class="col-md-12">
                <!---- Start Widget ---->
                <div class="widget wid-new-post">
                    <div class="heading"><h4>Journals</h4></div>


                    <div class="row">
                        <div class="col-sm-4 col-md-4">
                            <div class="thumbnail">
                                <img src="<?php echo base_url?>/resources/images/journal/1.jpg" alt="...">
                                <div class="caption" style="min-height: 250px">
                                    <h4>Arts & Humanities</h4>
                                    <p><a href="signIn.php" class="btn btn-info" role="button" style="width: 100%">See Papers</a></p>

                                    <p>Journal of Arts and Humanities (JAH) is a double-blind,peer-reviewed, open access refereed journal with an aim of becoming a leading journal in the arts and humanities disciplines. </p>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-md-4">
                            <div class="thumbnail">
                                <img src="<?php echo base_url?>/resources/images/journal/2.jpg" alt="...">
                                <div class="caption" style="min-height: 250px">
                                    <h4>Law</h4>
                                    <p><a href="signIn.php" class="btn btn-info" role="button" style="width: 100%">See Papers</a></p>
                                    <p>A scholarly or academic publication presenting commentary of emerging or topical developments in the law, and often specializing in a particular area of the law or legal information specific to a jurisdiction.</p>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-md-4">
                            <div class="thumbnail">
                                <img src="<?php echo base_url?>/resources/images/journal/3.jpg" alt="...">
                                <div class="caption" style="min-height: 250px">
                                    <h4>Medicine & Health</h4>
                                    <p><a href="signIn.php" class="btn btn-info" role="button" style="width: 100%">See Papers</a></p>
                                    <p>The Journal aims to publish original research article, review article, case reports, short communication in all aspects of Medical Research, Health Sciences, Clinical Research, Oncology, Biomedicine, Dentistry, Medical Education, Physiotherapy, Pharmacy and Nursing of such high quality</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-md-4">
                            <div class="thumbnail">
                                <img src="<?php echo base_url?>/resources/images/journal/4.jpg" alt="...">
                                <div class="caption" style="min-height: 250px">
                                    <h4>Science & Mathematics</h4>
                                    <p><a href="signIn.php" class="btn btn-info" role="button" style="width: 100%">See Papers</a></p>
                                    <p>Articles address common issues in mathematics and science education. The journal also publishes studies that explore the use of a variety of teaching and learning strategies in different cultural and cross-curricular contexts</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-md-4">
                            <div class="thumbnail">
                                <img src="<?php echo base_url?>/resources/images/journal/5.jpg" alt="...">
                                <div class="caption" style="min-height: 250px">
                                    <h4>Social Science</h4>
                                    <p><a href="signIn.php" class="btn btn-info" role="button" style="width: 100%">See Papers</a></p>
                                    <p>The principal purpose of the journal is to publish scholarly work in the social sciences defined in the classical sense, that is in the social sciences, the humanities, and the natural sciences. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                </div>
                <!---- Start Widget ---->
            </div>
        </div>
    </div>
</div>

<!-- Footer -->
<?php require_once('templateLayout/footer.php');?>
<!-- Footer -->
<!--script-->
<?php require_once('templateLayout/templateScript.php');?>
<!--script-->
</body>
</html>
