<?php
session_start();
require_once("../vendor/autoload.php");
require_once('templateLayout/information.php');

use App\model\AuthorUpPaper;
$object= new AuthorUpPaper();
$allData=$object->showAll();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="Free Bootstrap Themes by 365Bootstrap dot com - Free Responsive Html5 Templates">
    <meta name="author" content="http://www.365bootstrap.com">

    <title><?php echo $title?></title>

    <!-- Bootstrap Core CSS -->
    <?php require_once('templateLayout/templateCSS.php');?>
</head>

<body>
<header>
    <?php require_once('templateLayout/navigation.php');?>
</header>
<div class="featured container">
    <div class="row">
        <?php

        use App\Message\Message;


        if(isset($_SESSION) && !empty($_SESSION['message'])) {

            $msg = Message::getMessage();

            echo "
                        <div class='container'>
                            <div class='row'>
                                <div class='col-md-8 col-md-offset-2'>
                                    <p id='message' style='color: black; text-align: center; font-family: 'Times New Roman'; font-weight: 200 ;font-size: 20px;'><b>$msg</b></p>
                                </div>
                            </div>
                        </div>";
        }

        ?>
        <div class="col-sm-8">
            <!-- Carousel -->
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="<?php echo base_url; ?>resources/images/1.jpg" alt="First slide">
                        <!-- Static Header -->
                        <div class="header-text hidden-xs">
                            <div class="col-md-12 text-center">
                                <h2>Online Conference Management System</h2>
                                <br>
                                <h3>Journal paper</h3>
                                <br>
                            </div>
                        </div><!-- /header-text -->
                    </div>
                    <div class="item">
                        <img src="<?php echo base_url; ?>resources/images/2.jpg" alt="Second slide">
                        <!-- Static Header -->
                        <div class="header-text hidden-xs">
                            <div class="col-md-12 text-center">
                                <h2>Online Conference Management System</h2>
                                <br>
                                <h3>Journal paper</h3>
                                <br>
                            </div>
                        </div><!-- /header-text -->
                    </div>
                    <div class="item">
                        <img src="<?php echo base_url; ?>resources/images/3.jpg" alt="Third slide">
                        <!-- Static Header -->
                        <div class="header-text hidden-xs">
                            <div class="col-md-12 text-center">
                                <h2>Online Conference Management System</h2>
                                <br>
                                <h3>Journal paper</h3>
                                <br>
                            </div>
                        </div><!-- /header-text -->
                    </div>
                </div>
                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div><!-- /carousel -->
        </div>
        <div class="col-sm-4" >
            <div id="owl-demo-1" class="owl-carousel">
                <img src="<?php echo base_url; ?>resources/images/journal1.jpg" />
                <img src="<?php echo base_url; ?>resources/images/journal1.jpg" />
                <img src="<?php echo base_url; ?>resources/images/journal1.jpg" />
            </div>
            <img src="<?php echo base_url; ?>resources/images/journal2.jpg" />
        </div>
    </div>
</div>

<!-- /////////////////////////////////////////Content -->
<div id="page-content" class="index-page container">
    <div class="row">
        <div id="sidebar">
            <div class="col-md-12">
                <!---- Start Widget ---->
                <div class="widget wid-new-post">
                    <div class="heading"><h4>New Papers</h4></div>
                    <?php
                        foreach ($allData as $oneData){
                            echo "
                                
                            <div class='col-md-4'>
                                <div class='thumbnail'>
                                    <ul class='list-group'>
                                      <li class='list-group-item'>Topic : <b>$oneData->topic_category</b></li>
                                      <li class='list-group-item''>Topic's Title : <b>$oneData->topic</b></li>
                                      <li class='list-group-item'>Author's Name : <b>$oneData->name</b></li>
                                      <li class='list-group-item'><a href='signUp.php'>See Papers</a></li>
                                    </ul>
                                </div>
                            </div>
                            ";
                        }
                    ?>


                </div>
                <!---- Start Widget ---->
            </div>
        </div>
    </div>
</div>

<!-- Footer -->
<?php require_once('templateLayout/footer.php');?>
<!-- Footer -->
<!--script-->
<?php require_once('templateLayout/templateScript.php');?>
<!--script-->
</body>
</html>
