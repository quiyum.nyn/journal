<?php
require_once("../../vendor/autoload.php");
require_once('../templateLayout/information.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="Free Bootstrap Themes by 365Bootstrap dot com - Free Responsive Html5 Templates">
    <meta name="author" content="http://www.365bootstrap.com">

    <title><?php echo $title?></title>

    <!-- Bootstrap Core CSS -->
    <?php require_once('../templateLayout/templateCSS.php');?>
</head>

<body>
<header>
    <?php require_once('../templateLayout/navigation.php');?>
</header>


<!-- /////////////////////////////////////////Content -->
<div id="page-content" class="index-page container">
    <div class="row">
        <div id="main-content"><!-- background not working -->
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header header-vimeo">
                        <h2>Topics Category Name</h2>
                    </div>
                    <div class="box-content">
                        <div class="row" style="min-height: 400px;">
                            <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3" style="margin-top: 80px">
                                <form class="form-horizontal" action="<?php echo base_url; ?>controller/addtopicCategory.php" method="post">

                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="topics_category_name">Topics Category Name:</label>
                                        <div class="col-sm-8">
                                            <input type="topics_category_name" class="form-control" id="topics_category_name" placeholder="Enter Your Topics Category Name" name="topics_category_name" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-4 col-sm-8">
                                            <button type="submit" class="btn btn-info">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Footer -->
<?php require_once('templateLayout/footer.php');?>
<!-- Footer -->
<!--script-->
<?php require_once('templateLayout/templateScript.php');?>
<!--script-->
</body>
</html>
