<?php
session_start();
require_once("../vendor/autoload.php");
use App\model\AuthorUpPaper;
$object=new AuthorUpPaper();
if(isset($_FILES['file']['name']))
{
    $fileName=time().$_FILES['file']['name'];
    $tmp_name=$_FILES['file']['tmp_name'];

    move_uploaded_file($tmp_name,'../resources/papers/'.$fileName);
    $_POST['file_name']=$fileName;
}
$object->prepareData($_POST);
$object->store();