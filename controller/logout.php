<?php
if(!isset($_SESSION) )session_start();
include_once('../vendor/autoload.php');
use App\model\AuthorMaster;
use App\Message\Message;
use App\Utility\Utility;

$auth= new AuthorMaster();
$status= $auth->log_out();

session_destroy();
session_start();

Message::setMessage("Success! You Logout successfully!");
Utility::redirect('../views/signIn.php');