<?php
require_once("../vendor/autoload.php");
use App\model\AuthorMaster;
use App\model\ReviewerMaster;
use App\model\AdminMaster;
use App\Message\Message;
use App\Utility\Utility;
    if($_POST['role']=="author"){
        $object= new AuthorMaster();
        $object->prepareData($_POST);
        $verify=$object->loginCheck();
        $id=$object->viewId();
        if($verify){
            $_SESSION['role_status']=0;
            $_SESSION['email']=$_POST['email'];
            $_SESSION['id']=$id->userId;
            Message::setMessage("Success! You have log in Successfully!");
            Utility::redirect('../views/author/home.php');
        }
        else{
            Message::setMessage("Email & Password doesn't match!");
            Utility::redirect('../views/signIn.php');
        }
    }
    else if($_POST['role']=="reviewer"){
        $object= new ReviewerMaster();
        $object->prepareData($_POST);
        $verify=$object->loginCheck();
        $id=$object->viewId();
    
        if($verify){
            $_SESSION['role_status']=1;
            $_SESSION['email']=$_POST['email'];
            $_SESSION['id']=$id->userId;
            Message::setMessage("Success! You have log in Successfully!");
            Utility::redirect('../views/reviewer/home.php');
        }
        else{
            Message::setMessage("Email & Password doesn't match!");
            Utility::redirect('../views/signIn.php');
        }
    }
    else if($_POST['role']=="admin"){
        $object= new AdminMaster();
        $object->prepareData($_POST);
        $verify=$object->loginCheck();
        $id=$object->viewId();
        if($verify){
            $_SESSION['role_status']=2;
            $_SESSION['email']=$_POST['email'];
            $_SESSION['id']=$id->userId;
            Message::setMessage("Success! You have log in Successfully!");
            Utility::redirect('../views/admin/index.php');
        }
        else{
            Message::setMessage("Email & Password doesn't match!");
            Utility::redirect('../views/signIn.php');
        }
    }
    else if($_POST['role']=="selectOne"){
        Message::setMessage("Alert! Please select a role!");
        Utility::redirect('../views/signIn.php');
    }