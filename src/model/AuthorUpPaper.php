<?php
/**
 * Created by PhpStorm.
 * User: tusar.hasan
 * Date: 5/10/2017
 * Time: 11:29 PM
 */

namespace App\model;
use App\database\Database;
use PDO;
use App\Message\Message;
use App\Utility\Utility;

class AuthorUpPaper extends Database
{
    public $id;
    public $file_path;
    public $description;
    public $author_id;
    public $status;
    public $topic_cat_id;
    public $date;
    public $topicTitle;



    public function __construct(){
        parent::__construct();
    }

    public function prepareData($data){
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('file_name', $data)) {
            $this->file_path = $data['file_name'];
        }
        if (array_key_exists('topic_category', $data)) {
            $this->topic_cat_id = $data['topic_category'];
        }
        if (array_key_exists('auth_id', $data)) {
            $this->author_id = $data['auth_id'];
        }
        if (array_key_exists('title', $data)) {
            $this->topicTitle = $data['title'];
        }



        return $this;

    }
    public function store(){
        date_default_timezone_set('Asia/Dhaka');
        $date = date('Y-m-d H:i:s');
        $this->date=$date;
        $query= "INSERT INTO authors_up_paper (author_id,file_path,topic_cat_id,topic_title,date) VALUES (?,?,?,?,?)";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->author_id);
        $STH->bindParam(2,$this->file_path);
        $STH->bindParam(3,$this->topic_cat_id);
        $STH->bindParam(4,$this->topicTitle);
        $STH->bindParam(5,$this->date);

        $result = $STH->execute();
        if($result){

            Message::setMessage("Success! File successfully uploaded!");
        }
        else{
            Message::setMessage("Failed! File has not be uploaded!");
        }
        Utility::redirect('../views/author/uploadFile.php');
    }
    public function approve(){
        $this->status="1";
        $query= "UPDATE `authors_up_paper` SET status = ? WHERE `id`='$this->id'";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->status);

        $result = $STH->execute();
        if($result){

            Message::setMessage("Paper is approved!");
        }
        else{
            Message::setMessage("Failed!!!");
        }
        Utility::redirect('../views/admin/index.php');

    }
    public function reject(){
        $this->status="rejected";
        $query= "UPDATE `authors_up_paper` SET status = ? WHERE `id`='$this->id'";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->status);

        $result = $STH->execute();
        if($result){

            Message::setMessage("Paper is rejected!");
        }
        else{
            Message::setMessage("Failed!!!");
        }
        Utility::redirect('../views/admin/index.php');

    }

    public function show(){

        $sql = "SELECT a.id as id,b.name as name,a.date as date,b.email as email,a.topic_title as topic,a.`file_path` as path FROM `authors_up_paper`a,authors_master b,topic_category c WHERE a.`author_id`=b.id and a.`topic_cat_id`=c.id and status='0'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

    }
    public function showAll(){

        $sql = "SELECT a.id as id,b.name as name,a.date as date,b.email as email,a.topic_title as topic,a.`file_path` as path,c.`name` as topic_category FROM `authors_up_paper`a,authors_master b,topic_category c WHERE a.`author_id`=b.id and a.`topic_cat_id`=c.id and status='1'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

    }
    public function showArt(){

        $sql = "SELECT a.id as id,b.name as name,a.date as date,b.email as email,a.topic_title as topic,a.`file_path` as path FROM `authors_up_paper`a,authors_master b,topic_category c WHERE a.`author_id`=b.id and a.`topic_cat_id`=c.id and status='1' and topic_cat_id='1'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

    }
    public function showLaw(){

        $sql = "SELECT a.id as id,b.name as name,a.date as date,b.email as email,a.topic_title as topic,a.`file_path` as path FROM `authors_up_paper`a,authors_master b,topic_category c WHERE a.`author_id`=b.id and a.`topic_cat_id`=c.id and status='1' and topic_cat_id='2'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

    }
    public function showMed(){

        $sql = "SELECT a.id as id,b.name as name,a.date as date,b.email as email,a.topic_title as topic,a.`file_path` as path FROM `authors_up_paper`a,authors_master b,topic_category c WHERE a.`author_id`=b.id and a.`topic_cat_id`=c.id and status='1' and topic_cat_id='3'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

    }
    public function showScience(){

        $sql = "SELECT a.id as id,b.name as name,a.date as date,b.email as email,a.topic_title as topic,a.`file_path` as path FROM `authors_up_paper`a,authors_master b,topic_category c WHERE a.`author_id`=b.id and a.`topic_cat_id`=c.id and status='1' and topic_cat_id='4'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

    }
    public function showSocial(){

        $sql = "SELECT a.id as id,b.name as name,a.date as date,b.email as email,a.topic_title as topic,a.`file_path` as path FROM `authors_up_paper`a,authors_master b,topic_category c WHERE a.`author_id`=b.id and a.`topic_cat_id`=c.id and status='1' and topic_cat_id='5'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

    }
    public function showyourpapers(){

        $sql = "SELECT * FROM `authors_up_paper` WHERE `author_id`='$this->author_id' and status='1'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

    }


    public function showNumber(){

        $sql = "SELECT COUNT(*) as number FROM `authors_up_paper` WHERE `status`='0'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();

    }
    public function delete(){
        $sql = "DELETE from authors_up_paper WHERE id=".$this->id;

        $result= $this->DBH->exec($sql);


        if($result){
            Message::setMessage("Success! Data has been Deleted successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been Deleted!");
        }
        Utility::redirect('../views/admin/lessRated.php');
    }
    public function getMasterid(){
        $sql = "";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function viewSingleRow($id){
        $sql = "";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }

    public function showpaper(){
        $sql = "select * from authors_up_paper  WHERE `id`='$this->id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function update(){
        $query= 'UPDATE delivery_master SET status = ? WHERE id=?';

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$givedata);

        $result = $STH->execute();

    }
}