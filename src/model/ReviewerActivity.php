<?php
/**
 * Created by PhpStorm.
 * User: tusar.hasan
 * Date: 5/10/2017
 * Time: 11:31 PM
 */

namespace App\model;
use App\database\Database;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class ReviewerActivity extends  Database
{
    public $id;
    public $comment;
    public $author_up_id;
    public $status;
    public $reviewer_id;
    public $rating;

    public function __construct(){
        parent::__construct();
    }

    public function prepareData($data){
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('reviewer_id', $data)) {
            $this->reviewer_id = $data['reviewer_id'];
        }
        if (array_key_exists('auth_up_id', $data)) {
            $this->author_up_id = ($data['auth_up_id']);
        }
        if (array_key_exists('comment', $data)) {
            $this->comment = ($data['comment']);
        }
        if (array_key_exists('rating', $data)) {
            $this->rating = ($data['rating']);
        }
        return $this;

    }
    public function store(){

        $query= "INSERT INTO reviewer_activity (reviewer_id,athors_up_id,comments,rating) VALUES (?,?,?,?)";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->reviewer_id);
        $STH->bindParam(2,$this->author_up_id);
        $STH->bindParam(3,$this->comment);
        $STH->bindParam(4,$this->rating);

        $result = $STH->execute();
        if($result){

            Message::setMessage("Review Successfully!");
        }
        else{
            Message::setMessage("Failed!!!");
        }
        Utility::redirect('../views/reviewer/home.php');

    }
    public function update(){
        $query= 'UPDATE delivery_master SET status = ? WHERE id=?';

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$givedata);

        $result = $STH->execute();

    }
    public function show(){

        $sql = "";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

    }
    public function delete(){

    }
    public function getMasterid(){
        $sql = "";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function viewSingleRow($id){
        $sql = "";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function demo(){
        $sql = "select * from reviewer_activity  WHERE `athors_up_id`='$this->id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function showcomment(){
        $sql = "Select reviewer_activity.comments as com, reviewer_activity.reviewer_id as u_id,reviewer_activity.rating as rat,reviewer_master.name as name from reviewer_activity,reviewer_master where athors_up_id='$this->id' and reviewer_activity.reviewer_id= reviewer_master.id ORDER BY reviewer_activity.id DESC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showrating(){
        $sql = "Select sum(a.rating)/count(a.id) as avg_rating from reviewer_activity a,reviewer_master b where a.athors_up_id='$this->id' and a.reviewer_id= b.id";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function less_rating(){

        $sql = "SELECT * FROM `v1`  group by id HAVING COUNT(`id`) >1";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }


    public function isReviewExist(){
        $query = "SELECT * FROM `reviewer_activity` WHERE `reviewer_id`='$this->reviewer_id' and `athors_up_id`=$this->id";
        $STH=$this->DBH->query($query);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->fetchAll();


        $count = $STH->rowCount();
        if ($count > 0) {

            return TRUE;
        } else {
            return FALSE;
        }
    }
}